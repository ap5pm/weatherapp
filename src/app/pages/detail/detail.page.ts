import { Component, OnInit } from '@angular/core';
import { Weather } from 'src/app/Models/weather.model';
import { PlacesService } from 'src/app/services/places/places.service';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.page.html',
  styleUrls: ['./detail.page.scss'],
})
export class DetailPage implements OnInit {
  weather: Weather;

  constructor(private placesService: PlacesService) { 
  }

  ngOnInit() {
    this.weather = this.placesService.detail;
    console.log(this.weather);
  }

}
