import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from "@angular/common/http"
import { Observable } from 'rxjs';
import { Weather } from 'src/app/Models/weather.model';


@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) { }

  /**
   * Get Weather from API
   * 
   * @param latitude latitude of GEO
   * @param longitude longitude of GEO
   */
  getWeather(latitude: number, longitude: number): Observable<Weather> {
    // https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={API key}
    const url = environment.api.baseUrl + `/weather?lat=${latitude}&lon=${longitude}&appid=${environment.api.key}`;
    return this.http.get<Weather>(url);
  }
}
