import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { Weather } from '../Models/weather.model';
import { ApiService } from '../services/api/api.service';
import { ModalController } from '@ionic/angular';
import { SettingsPage } from '../pages/settings/settings.page';
import { PlacesService } from '../services/places/places.service';
import { StorageService } from '../services/storage/storage.service';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {
  weather$: Observable<Weather>[] = [];

  constructor(
    private modalCtrl: ModalController,
    private apiService: ApiService,
    private placesServices: PlacesService,
    private storageService: StorageService
  ) {
    this.initWeather();
  }

  async testStorage() {
    await this.storageService.saveData("test", {id: 1, content: "xyz"})
    const data = await this.storageService.getData("test")
    console.log(data)
  }

  async openModal() {
    const modal = await this.modalCtrl.create({
      component: SettingsPage,
    });
    modal.present();

    await modal.onWillDismiss();
    this.initWeather();
  }

  private initWeather() {
    this.placesServices.places$.subscribe(places => {
      this.weather$ = [];
      places.forEach(place => {
        if(place.homepage) {
          this.weather$.push(this.apiService.getWeather(place.latitude, place.longitude));
        }
      });
    })
  }

  openDetail(weather: Weather) {
    this.placesServices.detail = weather;
  }
}
